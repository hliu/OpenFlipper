if(LIBIGL_INCLUDE_DIR)
    set(LIBIGL_FOUND TRUE)
    set(LIBIGL_INCLUDE_DIRS "${LIBIGL_INCLUDE_DIR}")
else()
    find_path(LIBIGL_INCLUDE_DIR
        NAMES igl/igl_inline.h
        PATHS "${CMAKE_SOURCE_DIR}/libs/libigl/include")

    set(LIBIGL_INCLUDE_DIRS "${LIBIGL_INCLUDE_DIR}")

    include(FindPackageHandleStandardArgs)
    find_package_handle_standard_args(LIBIGL DEFAULT_MSG LIBIGL_INCLUDE_DIR)

    mark_as_advanced(LIBIGL_INCLUDE_DIR)
endif()
